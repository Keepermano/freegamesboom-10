from django.core.management.base import BaseCommand

from push.models import PushNotification


class Command(BaseCommand):

    def handle(self, *args, **options):
        for obj in PushNotification.objects.filter(processed=False, lock=False):
            obj.lock = True
            obj.save()
            result = obj.send_message()
            obj.report = str(result)
            obj.lock = False
            obj.processed = True
            obj.save()
