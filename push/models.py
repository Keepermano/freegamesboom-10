from django.contrib.contenttypes.models import ContentType
from django.db import models

from pyfcm import FCMNotification
from sorl.thumbnail import ImageField
from common.utils import upload_dir

from .settings import FCM_DJANGO_SETTINGS as SETTINGS


class PushUser(models.Model):
    token = models.CharField(unique=True, max_length=255)
    ip = models.CharField(blank=True, null=True, max_length=255)
    headers = models.TextField(blank=True)
    active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.token


class PushNotification(models.Model):
    title = models.CharField(max_length=100, verbose_name='Notification title')
    body = models.TextField(verbose_name='Notification body text')
    icon = ImageField(upload_to=upload_dir, blank=True, verbose_name='Notification icon')
    url = models.URLField(
        verbose_name='The action associated with a user click on the notification',
        help_text='For all URL values, HTTPS is required'
    )
    created = models.DateTimeField(auto_now_add=True)
    report = models.TextField(blank=True)
    lock = models.BooleanField(default=False, editable=False)
    processed = models.BooleanField(verbose_name='Processed', default=False)

    def __str__(self):
        return self.title

    def send_message(self):
        """
        Send notification for all active devices in queryset and deactivate if
        DELETE_INACTIVE_DEVICES setting is set to True.
        """
        push_service = FCMNotification(api_key=SETTINGS["FCM_SERVER_KEY"])
        registration_ids = list(PushUser.objects.filter(active=True).values_list(
            'token',
            flat=True
        ))
        if len(registration_ids) == 0:
            return [{'failure': 'No active users', 'success': 0}]

        result = push_service.notify_multiple_devices(
            registration_ids=registration_ids,
            message_title=self.title,
            message_body=self.body,
            message_icon='https://freegamesboom.com%s' % self.icon.url if self.icon else '',
            click_action=self.url,
        )
        self._deactivate_devices_with_error_results(
            registration_ids,
            result['results']
        )
        return result

    def _deactivate_devices_with_error_results(self, registration_ids, results):
        for (index, item) in enumerate(results):
            if 'error' in item:
                error_list = ['MissingRegistration', 'MismatchSenderId', 'InvalidRegistration', 'NotRegistered']
                if item['error'] in error_list:
                    registration_id = registration_ids[index]
                    PushUser.objects.filter(token=registration_id).update(
                        active=False
                    )
                    self._delete_inactive_devices_if_requested(registration_id)

    def _delete_inactive_devices_if_requested(self, registration_id):
        if SETTINGS["DELETE_INACTIVE_DEVICES"]:
            PushUser.objects.filter(token=registration_id).delete()
