# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-14 17:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: catalog/fields.py:23
msgid "Filetype not supported."
msgstr "Dosya turu desteklenmiyor."

#: catalog/templates/catalog/base.html:71
msgid "free games BOOM"
msgstr "free games BOOM"

#: catalog/templates/catalog/base.html:78
#: catalog/templates/catalog/base.html:133
msgid "Find more 100000 games"
msgstr "Daha fazla 100000 oyun bul"

#: catalog/templates/catalog/base.html:79
#: catalog/templates/catalog/base.html:134
#: catalog/templates/catalog/search.html:7
msgid "Search"
msgstr "Arama"

#: catalog/templates/catalog/base.html:97
#: catalog/templates/catalog/inclusions/sort_block.html:6
msgid "New"
msgstr "Yeni"

#: catalog/templates/catalog/base.html:103
msgid "Popular"
msgstr "Populer"

#: catalog/templates/catalog/base.html:114
msgid "Favourite <br>games"
msgstr "Favori <br> oyunlar"

#: catalog/templates/catalog/base.html:125
msgid "Last <br>played"
msgstr "Son <br> calınan"

#: catalog/templates/catalog/category.html:95
msgid "Information"
msgstr "Bilgi"

#: catalog/templates/catalog/game.html:90
msgid "Category"
msgstr "Kategori"

#: catalog/templates/catalog/game.html:105
msgid "Game"
msgstr "Oyun"

#: catalog/templates/catalog/game.html:131
#: catalog/templates/catalog/game.html:133
msgid "PLAY"
msgstr "OYNA"

#: catalog/templates/catalog/game.html:142
msgid "This game is not available on mobile."
msgstr "Bu oyun mobil cihazlarda kullanılamıyor."

#: catalog/templates/catalog/game.html:174
msgid "Controls"
msgstr "Kontroller"

#: catalog/templates/catalog/game.html:181
msgid "Video"
msgstr "Video"

#: catalog/templates/catalog/inclusions/_include_game_list.html:19
msgid "Play!"
msgstr "Oynayın!"

#: catalog/templates/catalog/inclusions/category_menu.html:9
msgid "TOP CATEGORIES"
msgstr "UST KATEGORILER"

#: catalog/templates/catalog/inclusions/game_block.html:5
msgid "votes"
msgstr "oylar"

#: catalog/templates/catalog/inclusions/game_block.html:11
msgid "Doesn’t  <br>work?"
msgstr "Calısmıyor <br>mu?"

#: catalog/templates/catalog/inclusions/game_block.html:17 catalog/views.py:226
msgid "Added"
msgstr "Katma"

#: catalog/templates/catalog/inclusions/game_block.html:17
msgid "Add to <br>favorites"
msgstr "<br> favorilerine ekle"

#: catalog/templates/catalog/inclusions/game_block.html:35
msgid "Share"
msgstr "Paylas"

#: catalog/templates/catalog/inclusions/game_block.html:41
msgid "Fullscreen"
msgstr "Tam ekran"

#: catalog/templates/catalog/inclusions/popup-games.html:7
msgid "Clear list"
msgstr "Listeyi temizle"

#: catalog/templates/catalog/inclusions/sort_block.html:3
msgid "Sort by"
msgstr "Gore sırala"

#: catalog/templates/catalog/inclusions/sort_block.html:3
#: catalog/templates/catalog/inclusions/sort_block.html:5
msgid "Most Popular"
msgstr "En populer"

#: catalog/templates/catalog/inclusions/sort_block.html:7
msgid "A-Z"
msgstr "A-Z"

#: catalog/templates/catalog/tags.html:6
msgid "Tags"
msgstr "Etiketler"

#: catalog/templates/catalog/tags.html:11
msgid "Popular tags"
msgstr "Populer etiketler"

#: catalog/templatetags/catalog_tags.py:45
msgid "MOST POPULAR GAMES"
msgstr "EN POPULER OYUNLAR"

#: catalog/templatetags/catalog_tags.py:64
msgid "POPULAR TAGS"
msgstr "POPULER ETIKETLER"

#: catalog/templatetags/catalog_tags.py:87
msgid "MOST POPULAR"
msgstr "EN POPULER"

#: catalog/templatetags/catalog_tags.py:156
msgid "FGM RECOMMENDED!"
msgstr "FGM ONERILIR!"

#: catalog/templatetags/catalog_tags.py:160
#, python-format
msgid "MORE %(category)s"
msgstr "DAHA %(category)s"

#: catalog/templatetags/catalog_tags.py:187
msgid "LAST PLAYED"
msgstr "SON CALINAN"

#: catalog/templatetags/catalog_tags.py:282 catalog/views.py:71
msgid "New games"
msgstr "Yeni oyunlar"

#: catalog/views.py:70
msgid "New games - Free Games Boom.com"
msgstr "Yeni oyunlar - Free Games Boom.com"

#: catalog/views.py:74
msgid "Popular games - Free Games Boom.com"
msgstr "Populer oyunlar - Free Games Boom.com"

#: catalog/views.py:75
msgid "Popular games"
msgstr "Populer oyunlar"

#: catalog/views.py:99
#, fuzzy, python-format
#| msgid "Popular games"
msgid "Similar %s games"
msgstr "Populer oyunlar"

#: catalog/views.py:191
msgid "Favourite games"
msgstr "Favori oyunlar"

#: catalog/views.py:194
msgid "Last Played"
msgstr "Son calınan"

#: flatpages/apps.py:7
msgid "Flat Pages"
msgstr "Duz Sayfalar"

#: flatpages/models.py:12
msgid "Title"
msgstr "Baslık"

#: flatpages/models.py:14
msgid "Text"
msgstr "Metin"

#: flatpages/models.py:15
msgid "Active"
msgstr "Aktif"

#: flatpages/models.py:16
msgid "Modified"
msgstr "Degistirilmis"

#: flatpages/models.py:19
msgid "Static pages"
msgstr "Statik sayfalar"

#: flatpages/models.py:20
msgid "Static page"
msgstr "Statik sayfa"

#: freegamesboom/settings.py:155
msgid "Afrikaans"
msgstr "Afrikaans"

#: freegamesboom/settings.py:156
msgid "Arabic"
msgstr "Arapca"

#: freegamesboom/settings.py:157
msgid "Azerbaijani"
msgstr "Azerbaycan"

#: freegamesboom/settings.py:158
msgid "Bulgarian"
msgstr "Bulgarca"

#: freegamesboom/settings.py:159
msgid "Belarusian"
msgstr "Belarusca"

#: freegamesboom/settings.py:160
msgid "Bengali"
msgstr "Bengalce"

#: freegamesboom/settings.py:161
msgid "Bosnian"
msgstr "Bosnalı"

#: freegamesboom/settings.py:162
msgid "Catalan"
msgstr "Katalan"

#: freegamesboom/settings.py:163
msgid "Czech"
msgstr "Cekce"

#: freegamesboom/settings.py:164
msgid "Welsh"
msgstr "Galce"

#: freegamesboom/settings.py:165
msgid "Danish"
msgstr "Danca"

#: freegamesboom/settings.py:166
msgid "German"
msgstr "Almanca"

#: freegamesboom/settings.py:167
msgid "Greek"
msgstr "Yunanca"

#: freegamesboom/settings.py:168
msgid "English"
msgstr "Ingilizce"

#: freegamesboom/settings.py:169
msgid "Esperanto"
msgstr "Esperanto"

#: freegamesboom/settings.py:170
msgid "Spanish"
msgstr "Ispanyolca"

#: freegamesboom/settings.py:171
msgid "Estonian"
msgstr "Estonya"

#: freegamesboom/settings.py:172
msgid "Finnish"
msgstr "Fince"

#: freegamesboom/settings.py:173
msgid "French"
msgstr "Fransız"

#: freegamesboom/settings.py:174
msgid "Irish"
msgstr "İrlandalı"

#: freegamesboom/settings.py:175
msgid "Galician"
msgstr "Galicya"

#: freegamesboom/settings.py:176
msgid "Hebrew"
msgstr "Ibranice"

#: freegamesboom/settings.py:177
msgid "Hindi"
msgstr "Hintce"

#: freegamesboom/settings.py:178
msgid "Croatian"
msgstr "Hırvatca"

#: freegamesboom/settings.py:179
msgid "Hungarian"
msgstr "Macarca"

#: freegamesboom/settings.py:180
msgid "Armenian"
msgstr "Ermeni"

#: freegamesboom/settings.py:182
msgid "Italian"
msgstr "İtalyanca"

#: freegamesboom/settings.py:183
msgid "Japanese"
msgstr "Japonca"

#: freegamesboom/settings.py:184
msgid "Georgian"
msgstr "Gurcistan"

#: freegamesboom/settings.py:186
msgid "Kazakh"
msgstr "Kazak"

#: freegamesboom/settings.py:187
msgid "Khmer"
msgstr "Khmer"

#: freegamesboom/settings.py:188
msgid "Kannada"
msgstr "Kannada"

#: freegamesboom/settings.py:189
msgid "Korean"
msgstr "Kore"

#: freegamesboom/settings.py:190
msgid "Luxembourgish"
msgstr "Luksemburgca"

#: freegamesboom/settings.py:191
msgid "Lithuanian"
msgstr "Litvanyalı"

#: freegamesboom/settings.py:192
msgid "Latvian"
msgstr "Letonyalı"

#: freegamesboom/settings.py:193
msgid "Macedonian"
msgstr "Makedon"

#: freegamesboom/settings.py:194
msgid "Malayalam"
msgstr "Malayalam"

#: freegamesboom/settings.py:195
msgid "Mongolian"
msgstr "Mogolca"

#: freegamesboom/settings.py:196
msgid "Marathi"
msgstr "Marathi"

#: freegamesboom/settings.py:197
msgid "Burmese"
msgstr "Birmanya"

#: freegamesboom/settings.py:198
msgid "Nepali"
msgstr "Nepalce"

#: freegamesboom/settings.py:199
msgid "Dutch"
msgstr "Hollandaca"

#: freegamesboom/settings.py:200
msgid "Ossetic"
msgstr "Osetya"

#: freegamesboom/settings.py:201
msgid "Punjabi"
msgstr "Punjabi"

#: freegamesboom/settings.py:202
msgid "Polish"
msgstr "Polonyalı"

#: freegamesboom/settings.py:203
msgid "Portuguese"
msgstr "Portekizce"

#: freegamesboom/settings.py:204
msgid "Romanian"
msgstr "Romanya"

#: freegamesboom/settings.py:205
msgid "Russian"
msgstr "Rusca"

#: freegamesboom/settings.py:206
msgid "Slovak"
msgstr "Slovakca"

#: freegamesboom/settings.py:207
msgid "Slovenian"
msgstr "Sloven"

#: freegamesboom/settings.py:208
msgid "Albanian"
msgstr "Arnavutluk"

#: freegamesboom/settings.py:209
msgid "Serbian"
msgstr "Sırpca"

#: freegamesboom/settings.py:210
msgid "Swedish"
msgstr "Isvecce"

#: freegamesboom/settings.py:211
msgid "Swahili"
msgstr "Svahili"

#: freegamesboom/settings.py:212
msgid "Tamil"
msgstr "Tamilce"

#: freegamesboom/settings.py:213
msgid "Telugu"
msgstr "Telugu"

#: freegamesboom/settings.py:214
msgid "Thai"
msgstr "Tay"

#: freegamesboom/settings.py:215
msgid "Turkish"
msgstr "Turk"

#: freegamesboom/settings.py:216
msgid "Tatar"
msgstr "Tatarlar"

#: freegamesboom/settings.py:218
msgid "Ukrainian"
msgstr "Ukrayna"

#: freegamesboom/settings.py:219
msgid "Urdu"
msgstr "Urduca"

#: freegamesboom/settings.py:220
msgid "Vietnamese"
msgstr "Vietnam"

#: freegamesboom/settings.py:221
msgid "Simplified Chinese"
msgstr "Basitlestirilmis Cince"

#: templates/404.html:18
msgid "404 Error"
msgstr "404 Error hata"

#: templates/404.html:19
msgid "Oops. This page does not exist."
msgstr "Oops. Bu sayfa mevcut degil."

#: templates/el_pagination/show_more.html:5
msgid "more"
msgstr "daha"

#: templates/el_pagination/show_pages.html:6
msgid "All"
msgstr "Tum"

#~ msgid "Indonesian"
#~ msgstr "Endonezya"

#~ msgid "Kabyle"
#~ msgstr "Kabyle"

#~ msgid "Udmurt"
#~ msgstr "Udmurt"
