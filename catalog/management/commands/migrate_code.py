# -*- coding: utf-8 -*-
import re

from django.core.management.base import BaseCommand
from tqdm import tqdm

from catalog.models import Game

site = 'http://www.agame.com'


class Command(BaseCommand):

    def handle(self, *args, **options):
        for o in tqdm(Game.objects.all()):
            if o.code:
                m = re.search('src="(.+?)" ', o.code)
                if m:
                    iframe_url = m.group(1)
                    o.link = iframe_url
                    o.save()
