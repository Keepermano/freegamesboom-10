# -*- coding: utf-8 -*-
import requests
import json

from django.core.management.base import BaseCommand


class Command(BaseCommand):

    def handle(self, *args, **options):

        headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:69.0) Gecko/20100101 Firefox/69.0',
            'Accept': 'application/json',
            'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
            'content-type': 'application/x-www-form-urlencoded',
            'Origin': 'https://gamedistribution.com',
            'DNT': '1',
            'Connection': 'keep-alive',
            'Referer': 'https://gamedistribution.com/',
        }

        params = (
            ('x-algolia-agent',
             'Algolia for JavaScript (3.34.0); Browser (lite); JS Helper (2.28.0); vue-instantsearch 1.7.0'),
            ('x-algolia-application-id', 'TQARMJGO8A'),
            ('x-algolia-api-key', '3690a3214c8934d794287d2d4c28d37f'),
        )
        res = []
        with open('gamedistribution.com.json', 'w') as f:
            for page in range(1, 20):
                data = '{"requests":[{"indexName":"prod_GAMEDISTRIBUTION","params":"query=&hitsPerPage=1000&page=' + str(page) + '&highlightPreTag=__ais-highlight__&highlightPostTag=__%2Fais-highlight__&filters=visible%3A1&facets=%5B%22tags%22%2C%22categories%22%2C%22mobile%22%2C%22company%22%2C%22type%22%5D&tagFilters="}]}'
                response = requests.post('https://tqarmjgo8a-dsn.algolia.net/1/indexes/*/queries', headers=headers,
                                         params=params, data=data)

                if response.status_code == 200:
                    data = response.json()['results'][0]
                    if data['hits']:
                        for x in data['hits']:
                            type = x['type']
                            try:
                                width = x['width']
                            except KeyError:
                                width = None
                            try:
                                height = x['height']
                            except KeyError:
                                height = None

                            try:
                                mobile = x['mobile']
                            except KeyError:
                                mobile = 0
                            try:
                                title = x['title']
                            except KeyError:
                                continue
                            description = x['description']
                            md5 = x['md5']
                            categories = x['categories']
                            tags = x['tags']
                            try:
                                image = 'https://img.gamedistribution.com/%s' % x['assets'][-1]['name']
                            except KeyError:
                                image = ''
                            res.append({
                                'height': height,
                                'width': width,
                                'mobile': mobile,
                                'type': type,
                                'title': title,
                                'description': description,
                                'md5': md5,
                                'categories': categories,
                                'tags': tags,
                                'image': image,
                            })
            json.dump(res, f, indent=4)
